@extends('layouts.main')

@section('title', 'Manage Cart')

@section('content')
@php($price = 0)
@php($total = 0)
<div class="container">
    <div class="d-flex flex-column align-items-start">
        <h1>My Cart</h1>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <td scope="col">Product Picture</td>
                    <td scope="col">Product name</td>
                    <td scope="col">Quantity</td>
                    <td scope="col">Price</td>
                    <td scope="col"></td>
                </tr>
            </thead>
            <tbody>
                @foreach($cart as $carts)
                    <tr>
                        <td><img src="{{asset('storage/'. $carts->figure->figurepict)}}" alt="" width="100px" height="100px"></td>
                        <td>{{$carts->figure->name}}</td>
                        <td><input type="text" value="{{$carts->quantity}}" disabled></td>
                        @php($price = $carts->quantity * $carts->figure->price)
                        <td>{{$price}}</td>
                        <td>
                            <form action="/cart/{{$carts->figure->id}}/delete" method="post">
                            @method('delete')
                            @csrf
                                <input type="submit" class="btn btn-danger" value="remove">
                            </form>
                        </td>
                    </tr>
                        @php($total += $price)
                @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td>total</td>
                        <td>Rp.{{$total}} </td>
                    </tr>
            </tbody>
        </table>
        <form action="/transaction" method="post">
            @csrf
            <input type="submit" value="Checkout" class="btn btn-success">
        </form>
    </div>
</div>
@endsection