@extends('layouts.main')

@section('title', 'Insert Feedback')

@section('content')
<div class="container d-flex flex-column align-items-center justify-content-center w-25">
    <h3>New Feedback</h3>
    <form action="/feedback/insert" method="post">
        @csrf
        <textarea name="feedback" cols="30" rows="10" class="form-control"></textarea>

        @if ($errors->has('description'))
            <span class="text-danger">{{ $errors->first('description') }}</span>
        @endif

        <button class="btn btn-primary mt-3" type="submit">Insert Feedback</button>
    </form>
</div>

@endsection