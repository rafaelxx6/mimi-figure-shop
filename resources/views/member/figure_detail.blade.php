@extends('layouts.main')

@section('title', 'Figure Detail')

@section('content')
<div class="container" style="display:flex;justify-content:space-evenly">
    <div style="width:25%;">
        <h1>{{$figure->name}}</h1>
        <img src="{{asset('storage/'. $figure->figurepict)}}" alt="{{$figure->figurepict}}" width="100%" height="350px">
    </div>
    <div style="width:35%; display:flex; flex-direction:column; justify-content:center; align-item:flex-start">
        <h1>Figure Description</h1>
        <p>{{$figure->description}}</p>
        <h1>Figure details</h1>
        <p>{{$figure->category->name}}</p>
        <p>{{$figure->quantity}} pcs</p>
        <p>Rp.{{$figure->price}}</p>
    </div>
</div>

@endsection