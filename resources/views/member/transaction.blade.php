@extends('layouts.main')

@section('title', 'My Transaction')

@section('content')
@php($price = 0)
@php($total = 0)
<div class="container">
    <h4>User Name: {{$transaction[0]->User->fullname}}</h4>
    @foreach($transaction as $header)
    <h4>Transaction Number : {{$header->id}}</h1>
    <h4>Transaction Date : {{$header->created_at->format('Y-m-d')}}</h4>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <td>figure picture</td>
                <td>figure name</td>
                <td>quantity</td>
                <td>Price</td>
            </tr>
        </thead>
        <tbody>
        @foreach($header->TransactionDetail as $detail)
            <tr>
                <td><img src="{{asset('storage/' . $detail->Figure->figurepict)}}" alt="{{$detail->Figure->figurepict}}" widht="100px" height="200px"></td>
                <td>{{$detail->Figure->name}}</td>
                <td>{{$detail->quantity}}</td>
                @php($price = $detail->quantity * $detail->figure->price)
                <td>{{$price}}</td>
            </tr>
            @php($total += $price)
        @endforeach
            <tr>
                <td></td>
                <td></td>
                <td>total</td>
                <td>Rp.{{$total}}</td>
            </tr>
        </tbody>
    </table>
    @endforeach
    {{$transaction->links()}}
</div>
@endsection