@extends('layouts.main')

@section('title', 'Home')

@section('content')

<div class="row d-flex align-content-center flex-column" style="width: 100%;">
  <div class=""style="padding-left: 30px;padding-right: 30px">
    @if(Auth::check())
      <h4>Welcome, {{Auth::user()->fullname}}</h4>
    @else
      <h4>Welcome, guest</h4>
    @endif
    <p id="displayTime"></p>
      <form action="/" method="get">
      @csrf
        <input type="text" name="search">
        <input type="submit" value="Search">
      </form>
  </div>

  <div class="col-7 d-flex flex-wrap mb-4" >
    @foreach($figure as $figures)
    <div class="col-4 d-flex justify-content-center align-items-center mt-5" >
      <div class="card" style="width: 100%">
        <img src="{{asset('storage/' . $figures->figurepict)}}" class="card-img-top" alt="" width="100px" height="150px">
        <div class="card-body">
          <h5 class="card-title"><a href="/detail/{{$figures->figure_id}}">{{$figures->figure_name}}</h5></a>
          <p class="card-text">{{$figures->description}}</p>
          @auth
            @if(Auth::user()->role == "member")
              <a href="/cart/{{$figures->figure_id}}" class="btn btn-success">Add to Cart</a>
            @endif
          @endauth
          <p>category: {{$figures->name}}</p>
        </div>
      </div>
    </div>
    @endforeach
  </div>
{{$figure->links()}}
</div>
@endsection