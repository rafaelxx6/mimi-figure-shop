@extends('layouts.main')

@section('title', 'Manage Feedback')

@section('content')
<div class="container">
    <div class="d-flex flex-column align-items-start">
        <h1>Manage Feedback</h1>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <td scope="col">Feedback Description</td>
                    <td scope="col">Status</td>
                    <td scope="col">Approve</td>
                    <td scope="col">Reject</td>
                </tr>
            </thead>
            <tbody>
                @foreach($feedback as $feedbacks)
                <tr>
                    <td>{{$feedbacks->description}}</td>
                    
                    @if($feedbacks->status == 1)
                    <td>Approved</td>
                    @else
                    <td>Rejected</td>
                    @endif

                    <form action="/manage-feedback/{{$feedbacks->id}}" method="post">
                    @csrf
                    <td><input class="btn btn-primary" name="status" type="submit" value="Approved"></input></td>
                    </form>
                    <form action="/manage-feedback/{{$feedbacks->id}}" method="post">
                    @csrf
                    <td><input class="btn btn-danger" name="status" type="submit" value="Rejected"></input></td>
                    </form>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection