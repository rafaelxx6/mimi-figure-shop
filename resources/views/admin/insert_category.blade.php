@extends('layouts.main')

@section('title','Insert Category')

@section('content')
<div class="container d-flex flex-column align-items-center justify-content-center w-25">
    <h3>Insert Figure Category </h3>
    <form action="/category/manage-category/insert" method="post">
        @csrf
        <input type="text" placeholder="Category Name" name="category" class="form-control @error('category') is-invalid @enderror">

        @if ($errors->has('category'))
            <span class="text-danger">{{ $errors->first('category') }}</span>
        @endif

        <button class="btn btn-primary mt-4" type="submit">Insert</button>
    </form>
</div>
@endsection