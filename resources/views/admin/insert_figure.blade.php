@extends('layouts.main')

@section('title', 'Insert Figure')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="row justify-content-center pb-4 pt-3">Insert</h1>
            <form method="POST" action="/figure/manage-figure/insert" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Figure Name" name="figurename" class="form-control @error('figurename') is-invalid @enderror">

                        @if ($errors->has('figurename'))
                            <span class="text-danger">{{ $errors->first('figurename') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <textarea placeholder="description" name="description" cols="30" rows="10"></textarea>

                        @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" placeholder="Quantity" name="quantity" class="form-control @error('quantity') is-invalid @enderror">

                        @if ($errors->has('quantity'))
                            <span class="text-danger">{{ $errors->first('quantity') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" placeholder="Price" name="price" class="form-control @error('quantity') is-invalid @enderror">

                        @if ($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select class="form-control" name="category">
                            <option value="">Select Category</option>
                            @foreach($category as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('category'))
                            <span class="text-danger">{{ $errors->first('category') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="exampleFormControlFile1">Figure Picture</label>
                        <input type="file" class="form-control-file" name ="figurepict" class="form-control @error('figurepict') is-invalid @enderror">
                    </div>
                    @if ($errors->has('figurepict'))
                        <span class="text-danger">{{ $errors->first('figurepict') }}</span>
                    @endif
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Insert') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection