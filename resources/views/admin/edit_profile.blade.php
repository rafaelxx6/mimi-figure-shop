@extends('layouts.main')

@section('title', 'Insert Figure')

@section('content')
<div class="container">
    <h1>Profile</h1>
    <hr>
    <div class="row d-flex justify-content-center fld-flex justify-content-center row">
    <h1 class="row justify-content-center pb-4 pt-3">Personal Info</h1>
        <div class=" col-md-10 d-flex  justify-content-around" >
            <div>
                <img src="{{asset('storage/' . $user->picture)}}" alt="{{$user->picture}}" width="200px" heigth="200px">
            </div>
            <div style="height:100%">
                <form method="POST" action="/profile/{{$user->id}}" enctype="multipart/form-data">
                    @method('patch')
                    @csrf
                    <div class="form-group">
                        <div class="col-md-12" >
                            <input type="text" placeholder="Full Name" name="fullname" value="{{$user->fullname}}" style="width:100%">

                            @if ($errors->has('fullname'))
                                <span class="text-danger">{{ $errors->first('fullname') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12" >
                            <input type="text" placeholder="Email" name="email" value="{{$user->email}}" style="width:100%;padding-left:0;padding-right:0;">

                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12" >
                            <input type="text" placeholder="Phone number" name="phonenumber" value="{{$user->phone}}"style="width:100%" >

                            @if ($errors->has('phonenumber'))
                                <span class="text-danger">{{ $errors->first('phonenumber') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" placeholder="Address" name="address" value="{{$user->address}}"style="width:100%">

                            @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-md-12">
                            <select name="gender" class="form-control">
                                <option value="">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            @if ($errors->has('gender'))
                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group  ">
                        <div class="col-md-12">
                            <label for="exampleFormControlFile1">Profile Picture</label>
                            <input type="file" class="form-control-file" name="profpict">
                        </div>
                        @if ($errors->has('profpict'))
                            <span class="text-danger">{{ $errors->first('profpict') }}</span>
                        @endif
                    </div>

                    <div class="col-md-8 form-group d-flex justify-content-center">
                        <div class="col-md-4 ">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Edit Profile') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection