@extends('layouts.main')

@section('title','Manage Figure')

@section('content')
<div class="container">
    <div class="d-flex flex-column align-items-start">
        <h1>Manage Figure</h1>
        <a href="/figure/manage-figure/insert" class="btn btn-primary">Insert New Figure</a>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <td scope="col">Figure Picture</td>
                    <td scope="col">Figure Name</td>
                    <td scope="col">Figure Category</td>
                    <td scope="col">Description</td>
                    <td scope="col">Quantity</td>
                    <td scope="col">Price</td>
                    <td scope="col">Edit</td>
                    <td scope="col">Delete</td>
                </tr>
            </thead>
            <tbody>
                @foreach($figures as $figure)
                <tr>
                    <th scope="row"><img src="{{asset('storage/'. $figure->figurepict)}}" width="100px" height="100px"></th>
                    <td>{{$figure->name}}</td>
                    <td>{{$figure->Category->name}}</td>
                    <td>
                        <p>{{$figure->description}}</p>
                    </td>
                    <td>{{$figure->quantity}} pcs</td>
                    <td>Rp.{{$figure->price}}</td>
                    <td><a href="/figure/edit/{{$figure->id}}" class="btn btn-success">edit</a></td>
                    <form action="/figure/{{$figure->id}}/delete" method="post">
                    @method('delete')
                    @csrf
                    <td><button class="btn btn-danger" type="submit">delete</button></td>
                    </form>
                </tr>
                @endforeach
            </tbody>
        </table>
                {{$figures->links()}}
    </div>
</div>
@endsection