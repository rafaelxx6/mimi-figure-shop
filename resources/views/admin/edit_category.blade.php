@extends('layouts.main')

@section('title','Update Category')

@section('content')
<div class="container d-flex flex-column align-items-center justify-content-center w-25">
    <h3>Update Figure Category </h3>
    <form action="/category/edit/{{$category->id}}" method="post">
        @method('PATCH')
        @csrf
        <input type="text" placeholder="Category Name" name="name" class="form-control @error('category') is-invalid @enderror" value="{{$category->name}}">

        @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif

        <button class="btn btn-primary mt-4" type="submit">Update</button>
    </form>
</div>
@endsection