@extends('layouts.main')

@section('title','Manage Figure')

@section('content')
<div class="container">
    <div class="d-flex flex-column align-items-start">
        <h1>Manage User</h1>
        <a href="/user/manage-user/insert" class="btn btn-primary">Insert New User</a>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <td scope="col">No</td>
                    <td scope="col">Profile Picture</td>
                    <td scope="col">Full Name</td>
                    <td scope="col">Email</td>
                    <td scope="col">Phone</td>
                    <td scope="col">Address</td>
                    <td scope="col">Gender</td>
                    <td scope="col">Role</td>
                    <td scope="col">Edit</td>
                    <td scope="col">Delete</td>
                </tr>
            </thead>
            <tbody>
                @foreach($user as $users)
                <tr>
                    <td>{{$users->id}}</td>
                    <td scope="row"><img src="{{asset('storage/'. $users->picture)}}" width="200px" height="200px"></td>
                    <td>{{$users->fullname}}</td>
                    <td>{{$users->email}}</td>
                    <td>{{$users->phone}}</td>
                    <td>{{$users->address}}</td>
                    <td>{{$users->gender}}</td>
                    <td>{{$users->role}}</td>
                    <td><a href="/user/edit/{{$users->id}}" class="btn btn-success">edit</a></td>
                    <td><form action="/user/{{$users->id}}/delete" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">delete</button></td>
                    </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection