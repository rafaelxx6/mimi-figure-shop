@extends('layouts.main')

@section('title', 'Transaction History')

@section('content')
@php($price = 0)
@php($total = 0)
<div class="container">
    @foreach($transaction->TransactionHeader as $header)
    <h4>User Name: {{$header->fullname}}</h4>
    <h4>Transaction Number :{{$header->user_id}}</h1>
    <h4>Transaction Date :{{$header->created_at->format('Y-m-d')}}</h4>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <td>figure picture</td>
                <td>figure name</td>
                <td>quantity</td>
                <td>Price</td>
            </tr>
        </thead>
        <tbody>
        @foreach($header->TransactionDetail as $detail)
            <tr>
                <td><img src="{{asset('storage/' . $detail->figurepict)}}" alt="{{$detail->figurepict}}" widht="100px" height="200px"></td>
                <td>{{$detail->name}}</td>
                <td>{{$detail->quantity}}</td>
                @php($price = $detail->quantity * $detail->figure->price)
                <td>{{$price}}</td>
            </tr>
            @php($total += $price)
        @endforeach
            <tr>
                <td></td>
                <td></td>
                <td>total</td>
                <td>Rp.{{$total}}</td>
            </tr>
        </tbody>
    </table>
    @endforeach
    {{$transaction->links()}}
</div>
@endsection