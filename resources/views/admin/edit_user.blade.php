@extends('layouts.main')

@section('title', 'Update User')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="row justify-content-center pb-4 pt-3">Update</h1>
            <form action="POST" action="/user/{{$user->id}}" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Full Name" name="fullname" value="{{$user->fullname}}">

                        @if ($errors->has('fullname'))
                            <span class="text-danger">{{ $errors->first('fullname') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Email" name="email" value="{{$user->email}}">

                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Phone number" name="phonenumber" value="{{$user->phone}}">

                        @if ($errors->has('phonenumber'))
                            <span class="text-danger">{{ $errors->first('phonenumber') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Address" name="address" value="{{$user->address}}">

                        @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select name="gender" class="form-control">
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        @if ($errors->has('role'))
                            <span class="text-danger">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select name="role" class="form-control">
                            <option value="">Select Role</option>
                            <option value="Admin">Admin</option>
                            <option value="Member">Member</option>
                        </select>
                        @if ($errors->has('role'))
                            <span class="text-danger">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="exampleFormControlFile1">Profile Picture</label>
                        <input type="file" class="form-control-file" name="profpict">
                        @if ($errors->has('profpict'))
                            <span class="text-danger">{{ $errors->first('profpict') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection