@extends('layouts.main')

@section('title', 'Manage Category')

@section('content')
<div class="container">
    <div class="d-flex flex-column align-items-start">
        <h1>Manage Category</h1>
        <a href="/category/manage-category/insert" class="btn btn-primary">Insert New Category</a>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <td scope="col">Category Name</td>
                    <td scope="col">Edit</td>
                    <td scope="col">Delete</td>
                </tr>
            </thead>
            <tbody>
                @foreach($category as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td><a href="/category/edit/{{$category->id}}" class="btn btn-success">edit</a></td>
                    <form action="/category/{{$category->id}}/delete" method="post">
                    @method('delete')
                    @csrf
                    <td><button type="submit" class="btn btn-danger">delete</button></td>
                    </form>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection