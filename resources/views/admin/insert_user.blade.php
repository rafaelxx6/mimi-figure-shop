@extends('layouts.main')

@section('title', 'Insert User')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="row justify-content-center pb-4 pt-3">Insert User</h1>
            <form method="POST" action="/user/manage-user/insert" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="FullName" name="fullname" class="form-control @error('fullname') is-invalid @enderror">

                        @if ($errors->has('fullname'))
                            <span class="text-danger">{{ $errors->first('fullname') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Email Address" name="email" class="form-control @error('email') is-invalid @enderror">

                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" placeholder="Password" name="password" class="form-control @error('password') is-invalid @enderror">

                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Phone number" name="phonenumber" class="form-control @error('phonenumber') is-invalid @enderror">

                        @if ($errors->has('phonenumber'))
                            <span class="text-danger">{{ $errors->first('phonenumber') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Address" name="address" class="form-control @error('address') is-invalid @enderror">

                        @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select class="form-control" aria-placeholder="gender" name="gender">
                            <option value="">Select Gender</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                        @if ($errors->has('gender'))
                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select class="form-control" name="role">
                            <option value="">Select Role</option>
                            <option value="admin">Admin</option>
                            <option value="member">Member</option>
                        </select>
                        @if ($errors->has('role'))
                            <span class="text-danger">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="exampleFormControlFile1">Profile Picture</label>
                        <input type="file" class="form-control-file" name="profpict" class="form-control @error('profpict') is-invalid @enderror">
                        
                        @if ($errors->has('profpict'))
                            <span class="text-danger">{{ $errors->first('profpict') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Insert') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection