@extends('layouts.main')

@section('title', 'Update Figure')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="row justify-content-center pb-4 pt-3">Update</h1>
            <form method="POST" action="/figure/edit/{{$figure->id}}" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" placeholder="Figure Name" name="figurename" value="{{$figure->name}}">

                        @if ($errors->has('figurename'))
                            <span class="text-danger">{{ $errors->first('figurename') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <textarea placeholder="description" name="description" cols="30" rows="10" value="{{$figure->description}}"></textarea>

                        @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" placeholder="Quantity" name="quantity" value="{{$figure->quantity}}">

                        @if ($errors->has('quantity'))
                            <span class="text-danger">{{ $errors->first('qunatity') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" placeholder="Price" name="price" value="{{$figure->price}}">

                        @if ($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <select class="form-control" name="category">
                            <option value="">Select Category</option>
                            @foreach($category as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        
                        @if ($errors->has('category'))
                            <span class="text-danger">{{ $errors->first('category') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="exampleFormControlFile1">Figure Picture</label>
                        <input type="file" class="form-control-file" name= "figurepict">

                        @if ($errors->has('figurepict'))
                            <span class="text-danger">{{ $errors->first('figurepict') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection