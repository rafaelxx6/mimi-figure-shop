<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//route that override auth route for homepage
Route::get('/', ['uses' => 'MemberController@index']);

//route for CRUD category in manage user
Route::group(['prefix' => 'user'], function () {
    Route::group(['middleware' => ['auth', 'admin:admin']], function() {
        Route::get('/manage-user', 'UserController@index');
        Route::get('/manage-user/insert', 'UserController@create');
        Route::post('/manage-user/insert', 'UserController@store');
        Route::get('/transaction-history', 'TransactionController@getTransactionHistory');
        Route::get('/edit/{id}', 'UserController@show');
        Route::patch('/edit/{id}', 'UserController@update');
        Route::delete('/{id}/delete', 'UserController@destroy');
    });
});

//route for CRUD category in manage figure
Route::group(['prefix' => 'figure'], function () {
    Route::group(['middleware' => ['auth', 'admin:admin']], function() {
        Route::get('/manage-figure', 'FigureController@index');
        Route::get('/manage-figure/insert', 'FigureController@create');
        Route::post('/manage-figure/insert', 'FigureController@store');
        Route::get('/edit/{id}', 'FigureController@show');
        Route::patch('/edit/{id}', 'FigureController@update');
        Route::delete('/{id}/delete', 'FigureController@destroy');
    });
});

//route for CRUD category in manage category
Route::group(['prefix' => 'category'], function () {
    Route::group(['middleware' => ['auth', 'admin:admin']], function() {
        Route::get('/manage-category', 'CategoryController@index');
        Route::get('/manage-category/insert', 'CategoryController@create');
        Route::post('/manage-category/insert', 'CategoryController@store');
        Route::get('/edit/{id}', 'CategoryController@show');
        Route::patch('/edit/{id}', 'CategoryController@update');
        Route::delete('/{id}/delete', 'CategoryController@destroy');
    });
});

Route::group(['middleware'=>['auth']], function() {
    //route for show form and insert feedback
    Route::get('/feedback/insert','FeedbackController@create');
    Route::post('/feedback/insert','FeedbackController@store');  
    
    //route for show, fetch figure to cart and remove a figure in a cart
    Route::get('/cart', 'CartController@showCart');
    Route::get('/cart/{id}', 'CartController@createCart');
    Route::delete('/cart/{id}/delete', 'CartController@deleteCart');
    
    // route for show and get all transaction from cart using checkout button for member
    Route::get('/transaction','TransactionController@getAllTransaction');
    Route::post('/transaction','TransactionController@transactionList');
    
    //route for show the profile page for admin and member and update they profile
    Route::get('/profile','UserController@showProfile');
    Route::patch('/profile/{id}', 'UserController@editProfile');
    
    //route to showall feedback and for make it approve or rejected
    Route::get('/manage-feedback','FeedbackController@index');
    Route::post('/manage-feedback/{id}','FeedbackController@changeStatus');
    
    //route for logout
    Route::get('/logout', 'UserController@logout');
});

//route to get detail for each figure in home
Route::get('/detail/{id}', 'MemberController@viewFigure');
//route to get search figure
Route::get('/', 'MemberController@index');

Auth::routes();