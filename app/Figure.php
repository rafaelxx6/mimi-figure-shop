<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    protected $fillable = [
        'name', 'description', 'quantity', 'price',
    ];

    public function Category()
    {
        return  $this->belongsTo('App\Category');
    }

    public function TransactionDetail()
    {
        return $this->hasMany('App\Figure');
    }
    public function Cart() 
    {
        return $this->hasMany('App\Cart');
    }
}
