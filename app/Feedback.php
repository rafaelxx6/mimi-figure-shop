<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'description', 'status'
    ];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
