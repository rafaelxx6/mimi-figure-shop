<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $fillable = [
        'quantity', 'transaction_id', 'figure_id'
    ];

    public function Figure()
    {
        return  $this->belongsTo('App\Figure');
    }
    public function TransactionHeader()
    {
        return $this->belongsTo('App\TransactionHeader','transaction_id');
    }
}
