<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function User() 
    {
        return $this -> belongsTo('App\User');
    }
    public function Figure() 
    {
        return $this -> belongsTo('App\Figure');
    }
}
