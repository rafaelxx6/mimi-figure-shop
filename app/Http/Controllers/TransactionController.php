<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TransactionHeader;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\User;
use App\Figure;
use App\TransactionDetail;

class TransactionController extends Controller
{
    public function getAllTransaction() 
    {   
        $transaction = TransactionHeader::with('User','TransactionDetail.Figure')->where('user_id', Auth::id())->paginate(1);

        return view('member.transaction', compact('transaction'));
    }
    
    public function getTransactionHistory() 
    {
        $transaction = User::with('TransactionHeader.TransactionDetail.Figure')->find(Auth::id())->paginate(1);
        //$transaction = TransactionHeader::with('User','TransactionDetail.Figure')->where('user_id', Auth::id())->paginate(1);

        return view('admin.transaction_history', compact('transaction'));
    }
    //Add transaction to transaction table in DB
    public function transactionList(Request $request) 
    {
        $transactionHeaders = new TransactionHeader();
        $transactionHeaders->user_id = Auth::id();
        $transactionHeaders->save();

        if($transactionHeaders) {
            $cart = Cart::where('user_id', Auth::id())->get();

            foreach($cart as $carts) 
            {
                $transactionDetail = new TransactionDetail();
                $transactionDetail->figure_id = $carts->figure_id;
                $transactionDetail->quantity = $carts->quantity;
                $transactionDetail->transaction_id = $transactionHeaders->id;
                
                $figure = Figure::findorfail($transactionDetail->figure_id);
                $figure->quantity -= $transactionDetail->quantity;
                $figure->save();

                $transactionDetail->save();
            }
        }
        $cart = Cart::where('user_id',Auth::id());
        $cart->delete();

        return redirect('/transaction');
    }
}
