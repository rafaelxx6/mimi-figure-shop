<?php

namespace App\Http\Controllers;

use App\Category;
use Validator;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display all category in manage category.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('admin.manage_category', compact('category'));
    }

    /**
     * Show the form for insert category in manage category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.insert_category');
    }

    /**
     * Store a newly created category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'category' => 'required|string|min:5'
            ]
        );
        if ($validator->fails()) {
            return redirect('/category/manage-category/insert')
            ->withErrors($validator)
            ->withInput();
        }

        $category = new Category();
        $category->name = $request['category'];

        $category->save();

        return redirect('/category/manage-category');
    }

    /**
     * Display the specified category and show the form for edit .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return view ('admin.edit_category', compact('category'));
    }

    /**
     * Update the specified category in manage category.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'category' => 'required|min:5|string'
            ]
        );

        if ($validator->fails()) {
            return redirect('/category/edit/{id}')
            ->withErrors($validator)
            ->withInput();
        }

        $category = Category::find($id);
        $category->name = $request->name;
        $category -> save();

        return redirect('/category/manage-category');
    }

    /**
     * Remove the specified category from storage in manage category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if ($category != null) {
            $category->delete();
            return redirect('/category/manage-category');
        }

        return redirect('/category/manage-category');
    }
}
