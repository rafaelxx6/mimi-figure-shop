<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Figure;
use App\User;
use DB;
use Illuminate\Suppport\Facades\Auth;

class MemberController extends Controller
{
    //Show all figure in home page and function to search
    public function index(Request $request) 
    {
        $query = $request->get('search');
        $figure = DB::table('figures')->join('categories','categories.id','=','figures.category_id')
        ->where('figures.name', 'LIKE', '%' . $query .'%')->orWhere('categories.name', 'LIKE', '%'. $query .'%')
        ->select('figures.name as figure_name','figures.id as figure_id','description', 'quantity', 'figurepict', 'price', 'categories.name')
        ->paginate(6);
        
        return view('member.home_member',compact('figure'));
    }
    //Show specified figure
    public function viewFigure($figure_id) 
    {
        $figure = Figure::where('id', $figure_id)->first();
        return view('member.figure_detail', compact('figure'));
    }
}
