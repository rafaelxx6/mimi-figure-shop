<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Feedback;

class FeedbackController extends Controller
{
    //Show all feedback for manage feedback in admin 
    public function index()
    {
        $feedback = Feedback::all();
        return view('admin.manage_feedback', compact('feedback'));
    }
    
    //show the form for insert feedback
    public function create() {
        return view('member.insert_feedback');
    }
    
    //Store a feedback to DB
    public function store(Request $request) 
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'description' => 'required|min:10|string'
            ]
        );

        if($validator->fails()){
            return redirect('/feedback/insert')
                ->withErrors($validator)
                ->withInput();
        }

        $feedback = new Feedback();
        $feedback->description = $request['feedback'];
        $feedback->user_id = Auth::id();

        $feedback->save();
        return redirect('/view');
    }

    //Change status for feedback
    public function changeStatus(Request $request, $id) 
    {
        $status_approved = 1;
        $status_rejected = 0;
        
        $feedback = Feedback::find($id);

        if($request['status'] == "Approved"){
            $feedback->status = $status_approved;
        }else if($request['status'] == "Rejected") {
            $feedback->status = $status_rejected;
        }

        $feedback->save();
        return redirect('/manage-feedback');
    }
}
