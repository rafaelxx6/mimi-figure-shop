<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Storage;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display All User For Admin
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('admin.manage_user', compact('user'));
    }

    /**
     * Show the form to Insert User For Admin
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.insert_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'fullname' => 'required|string|min:5',
                'email' => 'required|unique:users|e-mail',
                'phonenumber' => 'required|numeric|min:11',
                'password' => 'required|alpha_num|min:5|string',
                'address' => 'required|string|min:10',
                'gender' => 'required',
                'role' => 'required',
                'profpict' => 'required|mimes:jpeg,jpg,png'
            ]
        );

        if($validator->fails()) {
            return redirect('/user/manage-user/insert')
                ->withErrors($validator)
                ->withinput();
        }

        $file = $request->file('profpict');
        $filename = 'user-' . $request['fullname'] . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public', $filename);

        $user = new User();
        $user->fullname = $request['fullname'];
        $user->phone = $request['phonenumber'];
        $user->email = $request['email'];
        $user->password = Hash::Make($request['password']);
        $user->address = $request['address'];
        $user->gender = $request['gender'];
        $user->role = $request['role'];
        $user->picture = $filename;

        $user->save();
        return redirect('/user/manage-user');
    }

    /**
     * Display the form for specified user that has been selected
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= User::find($id);
        return view('admin.edit_user', compact('user'));
    }

    /**
     * Update the specified user 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'fullname' => 'required|string|min:5',
                'email' => 'required|unique|e-mail',
                'phonenumber' => 'required|numeric|min:11',
                'address' => 'required|string|min:10',
                'gender' => 'required',
                'role' => 'required',
                'profpict' => 'required|mimes:jpeg,jpg,png'
            ]
        );

        if($validator->fails()) {
            return redirect('/user/edit/{id}')
                ->withErrors($validator)
                ->withinput();
        }

        $file = $request->file('profpict');
        $filename = 'user-' . $request['fullname'] . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public', $filename);
        
        $user = User::find($id);
        $oldpath = 'public/' . $user->picture;
        Storage::delete($oldpath);
        $user->picture = $filename;
        
        $user->fullname = $request['fullname'];
        $user->phone = $request['phonenumber'];
        $user->email = $request['email'];
        $user->address = $request['address'];
        $user->gender = $request['gender'];
        $user->role = $request['role'];
        
        $user->save();
        return redirect('/user/manage-user');
    }

    /**
     * Remove the specified user 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $oldpath='/public/'.$user->picture;
        Storage::delete($oldpath);

        $user->delete();
        return redirect('/user/manage-user');
    }

    //Display the profile for the logged in user
    public function showProfile() 
    {
        $id = Auth::id();
        $user = User::findorfail($id);
        return view('admin.edit_profile', compact('user'));
    }
    
    //Edit the profile for the logged in user
    public function editProfile(Request $request, $id) 
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'fullname' => 'required|string|min:5',
                'email' => 'required|unique|e-mail',
                'phonenumber' => 'required|numeric|min:11',
                'address' => 'required|string|min:10',
                'gender' => 'required',
                'role' => 'required',
                'profpict' => 'required|mimes:jpeg,jpg,png'
            ]
        );
        
        if($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withinput();
        }
            
        $file = $request->file('profpict');
        $filename = 'user-' . $request['fullname'] . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public', $filename);
        
        $user = User::find($id);
        $oldpath = 'public/' . $user->picture;
        Storage::delete($oldpath);
        $user->picture = $filename;
        
        $user->fullname = $request['fullname'];
        $user->phone = $request['phonenumber'];
        $user->email = $request['email'];
        $user->address = $request['address'];
        $user->gender = $request['gender'];
        $user->picture = $filename;
        
        $user->save();
        return redirect('/');
    }
    //Override function logout from auth
    public function logout() 
    {
        Auth::logout();

        return redirect('/');
    }
    //Default constructor if user controller is call
    public function __construct()
   {
       $this->middleware('admin');
   }
}
