<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Figure;
use App\Category;
use Illuminate\Support\Facades\Validator;

class FigureController extends Controller
{
    /**
     * Display all figure for manage Figure
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $figures = Figure::paginate(6);
        return view('admin.manage_figure', compact('figures'));
    }

    /**
     * Show all category for insert figure
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.insert_figure', compact('category'));
    }

    /**
     * Store a newly created figure to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'figurename' => 'required | string | min:5 ',
                'description' => 'required | string | min: 10',
                'quantity' => 'required|numeric|min:1',
                'price' => 'required|min:100000 |numeric',
                'category' => 'required',
                'figurepict' => 'required | mimes:jpeg,png,jpg'
            ]
        );

        if ($validator->fails()) {
            return redirect('/figure/manage-figure/insert')
                ->withErrors($validator)
                ->withInput();
        }

        $figure = new Figure();
        $figure->name = $request['figurename'];
        $figure->description = $request['description'];
        $figure->quantity = $request['quantity'];
        $figure->price = $request['price'];
        $figure->category_id = $request['category'];
        
        $file = $request->file('figurepict');
        $filename = 'figure-'.$request['figurename'].'.'.$file->getClientOriginalExtension();
        $path = $file->storeAs('public',$filename);
        $figure->figurepict = $filename;
        
        $figure->save();
        return redirect('/figure/manage-figure');
    }
    
    /**
     * Display the specified figure and show the form for edit.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $figure = Figure::find($id);
        $category = Category::all();
        return view('admin.edit_figure', compact('category', 'figure'));
    }
    
    /**
     * Update the specified figure in manage Figure.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'figurename' => 'required | string | min:5 ',
                'description' => 'required | string | min: 10',
                'quantity' => 'required | numeric | min:1 ',
                'price' => 'required | min:100000 | numeric',
                'category' => 'required',
                'figurepict' => 'required | mimes:jpeg, png, jpg'
            ]
        );
            
        if ($validator->fails()) {
            return redirect('/figure/manage-figure')
                ->withErrors($validator)
                ->withInput();
        }
                
        $file = $request->file('figurepict');
        $filename = 'figure-'.$request['figurename'].'.'.$file->getClientOriginalExtension();
        $path = $file->storeAs('public',$filename);
                
        $figure = Figure::find($id);
        $oldpath = 'public/'.$figure->figurepict;
        Storage::delete($oldpath);       
        $figure->figurepict=$filename;
                
        $figure->name = $request['figurename'];
        $figure->description = $request['description'];
        $figure->quantity = $request['quantity'];
        $figure->price = $request['price'];
        $figure->category_id = $request['category'];
                
        $figure->save();
        return redirect('/figure/manage-figure');
    }

    /**
     * Remove the specified figure in manage figure.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $figure = Figure::find($id);
        $oldpath='/public/'.$figure->figurepict;
        Storage::delete($oldpath);

        $figure->delete();
        return redirect('/figure/manage-figure');
    }
}
