<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Figure;

class CartController extends Controller
{
    //Show all cart in a cart page
    public function showCart()
    {
        $cart = Cart::with('figure')->where('user_id', Auth::id())->paginate(6);
        return view('member.cart_page', compact('cart'));
    }

    //Create or insert to cart from add to cart button
    public function createCart($figure_id) 
    {
        $cart = Cart::where('figure_id', $figure_id)-> first();

        if($cart) {
            $cart->quantity+=1;
        }else {
            $cart = new cart();
            $cart->figure_id = $figure_id;
            $cart->user_id = Auth::id();
            $cart->quantity = 1;
        }

        $cart-> save();

        $cart = Cart::with('figure')->where('user_id', Auth::id())->paginate(6);
        return redirect('/cart');
    }
    //Delete a specific item that is inserted to cart before
    public function deleteCart($figure_id) {
        $cart = Cart::where('figure_id',$figure_id)->first();
        $cart->delete();

        $cart = Cart::with('figure')->where('user_id', Auth::id())->paginate(6);
        return redirect('/cart');
    }

}
